<?php

namespace Drupal\apcu_cache_clear\Commands;

use Drush\Commands\DrushCommands;

/**
 * @file
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */

/**
 * ApcuClear class extension.
 */
class ApcuClear extends DrushCommands {

  /**
   * Echos back hello with the argument provided.
   *
   * @command apcu:clear
   * @aliases apcuc
   * @options arr An option that takes multiple values.
   * @usage apcu:clear
   * .
   */
  public function clear() {
    if (function_exists('apc_clear_cache')) {
      apc_clear_cache();
      apc_clear_cache('user');
      apc_clear_cache('opcode');

      $this->output()->writeln('APCu cache was cleared.');
    }
    else {
      $this->output()->writeln('The function apc_clear_cache not exists.');
    }
    if (function_exists('apcu_clear_cache')) {
      apcu_clear_cache();

      $this->output()->writeln('APCu cache was cleared.');
    }
    else {
      $this->output()->writeln('The function apcu_clear_cache not exists.');
    }

  }

}
