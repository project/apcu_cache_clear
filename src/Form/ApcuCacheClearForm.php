<?php

namespace Drupal\apcu_cache_clear\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Class ApcuCacheClearForm extension.
 */
class ApcuCacheClearForm extends FormBase {

  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apcu_cache_clear_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['clear_cache'] = [
      '#type' => 'details',
      '#title' => $this->t('Clear APCu cache'),
      '#open' => TRUE,
    ];

    $form['clear_cache']['help'] = [
      '#type' => 'item',
      '#title' => $this->t('Issue description:'),
      '#markup' => $this->t('<p>In drupal 7 when you moved modules from one directory to other, usually running drush rr (registry rebuild) would work, maybe you would need to clear caches. In drupal 8, this wont help. Paths are saved in APC and you will probably need to restart server.</p>
<p><a target="_blank" href="https://www.drupaldump.com/changingmoving-modules-path-drupal-8">Issue description.</a></p>
<p><a target="_blank" href="https://support.acquia.com/hc/en-us/articles/360056721853-Clearing-PHP-Apcu-cache-on-Acquia-Cloud-">Issue solution.</a></p>
<p><a target="_blank" href="https://www.php.net/manual/en/function.apcu-clear-cache.php">apcu_clear_cache() documentation.</a></p>'),
    ];

    $form['clear_cache']['clear_apcu_cache'] = [
      '#type' => 'submit',
      '#title' => $this->t('Clear APCu cache'),
      '#description' => $this->t('This button invalidates the APCu cache on the server.'),
      '#default_value' => 'Clear APCu cache',
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if (function_exists('apc_clear_cache')) {
      apc_clear_cache();
      apc_clear_cache('user');
      apc_clear_cache('opcode');

      $this->messenger()->addMessage('APCu cache was cleared.');
    }
    else {
      $this->messenger()->addError('The function apc_clear_cache not exists.');
    }

    if (function_exists('apcu_clear_cache')) {
      apcu_clear_cache();

      $this->messenger()->addMessage('APCu cache was cleared.');
    }
    else {
      $this->messenger()->addError('The function apcu_clear_cache not exists.');
    }
  }

}
