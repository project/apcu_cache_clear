# INTRODUCTION
------------
PHP APCu cache clear helper.
In drupal 7 when you moved modules from one directory to other, usually running drush rr (registry rebuild) would work, maybe you would need to clear caches. In drupal 8, this won't help. Paths are saved in APC and you will probably need to restart server.

This module allows to:
* clear APCu cache by means of Drupal admin panel
    * go to `/admin/config/development/performance/apcu` and press "Clear APCu cache" button
* clear APCu cache by means of drush CLI:
    * `drush apcu:clear` or `drush apcuc`
    
![](https://img.shields.io/static/v1?label=&message=Warning:&color=red) Please pay attention: the APCu cache will be cleared for all sites on the server!
# REQUIREMENTS
------------
* https://www.php.net/manual/en/book.apcu.php

# INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module.
 * Install by means of Drush CLI: `drush pm:enable apcu_cache_clear`

# CONFIGURATION
-------------
This module does not have any additional configuration requirements.

# REFERENCES
-----------
* https://www.drupaldump.com/changingmoving-modules-path-drupal-8 - Issue description.
* https://support.acquia.com/hc/en-us/articles/360056721853-Clearing-PHP-Apcu-cache-on-Acquia-Cloud- - Issue solution.
* https://www.php.net/manual/en/function.apcu-clear-cache.php - apcu_clear_cache() documentation.

# MAINTAINERS
-----------

Current maintainers:
 * Ievgen Kyvgyla <hello@kivgila.pro> - https://kivgila.pro

This project has been sponsored by:
 * EPAM Systems <https://www.epam.com/>
